import React, { Component } from "react";
import Cell from "./cell";
import croc from "./croc.svg";
import elephant from "./elephant.svg";
import giraffe from "./giraffe.svg";
import gorilla from "./gorilla.svg";
import koala from "/.koala.svg";
import polarbear from "./polar-bear.svg";
import tiger from "./tiger.svg";
import whale from "./whale.svg";
class MainComponent extends Component {
  state = {
    cells: [
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
    ],
    images: [croc, elephant, giraffe, gorilla, koala, polarbear, tiger, whale],
  };

  handleGrid = (item) => {
    console.log(item);
    let arr = [...this.state.cells];
    console.log(arr[item], "item");

    this.setState({
      cells: arr,
    });
  };

  handleResetbtn = () => {
    this.setState({ cells: this.state.cells.fill(null) });
  };
  handleNewGamebtn = () => {
    this.setState({
      cells: this.state.cells.fill(null),
      countXor0: 0,
      xisNext: true,
    });
  };

  render() {
    console.log(this.state.images);
    //<img src={croc} className="App-logo" alt="croc" />
    return (
      <div className="container">
        <h3 className="text-center">{this.handleMove()} </h3>

        <div className="row justify-content-center">
          <Cell
            value={this.state.cells[0]}
            onClickbtn={() => this.handleGrid(0)}
          ></Cell>
          <Cell
            value={this.state.cells[1]}
            onClickbtn={() => this.handleGrid(1)}
          ></Cell>
          <Cell
            value={this.state.cells[2]}
            onClickbtn={() => this.handleGrid(2)}
          ></Cell>
          <Cell
            value={this.state.cells[3]}
            onClickbtn={() => this.handleGrid(3)}
          ></Cell>
        </div>

        <div className="row justify-content-center">
          <Cell
            value={this.state.cells[4]}
            onClickbtn={() => this.handleGrid(4)}
          ></Cell>
          <Cell
            value={this.state.cells[5]}
            onClickbtn={() => this.handleGrid(5)}
          ></Cell>
          <Cell
            value={this.state.cells[6]}
            onClickbtn={() => this.handleGrid(6)}
          ></Cell>
          <Cell
            value={this.state.cells[7]}
            onClickbtn={() => this.handleGrid(7)}
          ></Cell>
        </div>

        <div className="row justify-content-center">
          <Cell
            value={this.state.cells[8]}
            onClickbtn={() => this.handleGrid(8)}
          ></Cell>
          <Cell
            value={this.state.cells[9]}
            onClickbtn={() => this.handleGrid(9)}
          ></Cell>
          <Cell
            value={this.state.cells[10]}
            onClickbtn={() => this.handleGrid(10)}
          ></Cell>
          <Cell
            value={this.state.cells[11]}
            onClickbtn={() => this.handleGrid(11)}
          ></Cell>
        </div>
        <div className="row justify-content-center">
          <Cell
            value={this.state.cells[12]}
            onClickbtn={() => this.handleGrid(12)}
          ></Cell>
          <Cell
            value={this.state.cells[13]}
            onClickbtn={() => this.handleGrid(13)}
          ></Cell>
          <Cell
            value={this.state.cells[14]}
            onClickbtn={() => this.handleGrid(14)}
          ></Cell>
          <Cell
            value={this.state.cells[15]}
            onClickbtn={() => this.handleGrid(15)}
          ></Cell>
        </div>
        <div className="text-center m-1">{this.showButton()}</div>
      </div>
    );
  }
  handleMove() {
    let checkNull = this.state.cells.find((str) => str === null);
    console.log(checkNull, "null");
    if (checkNull === null) return "";
    return "Game Over";
  }
  showButton() {
    if (this.state.countXor0 === 9) {
      return (
        <React.Fragment>
          <button
            className="btn btn-primary"
            onClick={() => this.handleNewGamebtn()}
          >
            New Game
          </button>
        </React.Fragment>
      );
    } else {
      return (
        <React.Fragment>
          <button
            className="btn btn-primary "
            onClick={() => this.handleResetbtn()}
          >
            Reset Game
          </button>
        </React.Fragment>
      );
    }
  }
}

export default MainComponent;
