import React, { Component } from "react";
class Cell extends Component {
  render() {
    return (
      <React.Fragment>
        <div
          className="col-"
          style={{
            width: "120px",
            height: "120px",
            fontSize: "80px",
            textAlign: "center",
            backgroundColor: "#04a6e0",
            margin: "2px",
            padding: "1px",
            borderRadius: "4px",
          }}
          onClick={() => this.props.onClickbtn()}
        >
          {this.props.value}
        </div>
      </React.Fragment>
    );
  }
}

export default Cell;
